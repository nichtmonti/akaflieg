<?php
require('php/head.php');
?>
<title>Baustunden einsehen</title>
</head>
<?php
require_once('db/database_connect.php');
require('php/menu.php');
require('php/make_table.php');
$ID=$_POST["ID"];

//determine name and time by ID
$link=connect();

$sql = "SELECT vorname,spitzname,nachname , sum(eintrag.Arbeitszeit)%60 as \"Minuten\",FLOOR(sum(eintrag.Arbeitszeit)/60) as \"Stunden\" FROM mitglieder left JOIN eintrag ON eintrag.Mitglied_ID=mitglieder.ID WHERE mitglieder.ID like ?";
$stmt=mysqli_stmt_init($link);
$stmt->prepare($sql);
$stmt->bind_param("i",$ID);
$stmt->execute();
$stmt->bind_result($vorname,$spitzname,$nachname,$min,$hr);
$stmt->fetch();
$stmt->close();

if(is_null($spitzname)){
    $name=$vorname." ".$nachname;
}
else{
    $name=$vorname." \"".$spitzname." \"".$nachname;
}
//get table with work times

$stmt= mysqli_stmt_init($link);
/*$sql = "SELECT eintrag.Datum, eintrag.Beschreibung, projekt.Name, format_time(eintrag.Arbeitszeit) as \"Zeit\"
  FROM eintrag JOIN mitglieder ON eintrag.Mitglied_ID=mitglieder.ID JOIN projekt on eintrag.Projekt_ID=projekt.ID JOIN projekt_helfer ON eintrag.ID=projekt_helfer.eintrag_ID WHERE mitglieder.ID LIKE ? ORDER BY Datum DESC";*/

$sql= "SELECT Datum, Mitarbeiter, Beschreibung, Projekt,Arbeitszeit FROM full_view_by_id WHERE mitglied_ID Like ?";

$stmt->prepare($sql);
$stmt->bind_param("i",$ID);
$stmt->execute();
$res=$stmt->get_result();

if($res->num_rows<1) {
    echo("<h4>Noch keine Einträge für $name vorhanden</h4>");
}

else {
    echo ("<h2 style='text-align:center;'> Baustunden für $name</h2><h3> Gesamtzeit: $hr Stunden und $min Minuten</h3>");
    table($res);}

$stmt->close();
disconnect($link);
?>
</body>
</html>
