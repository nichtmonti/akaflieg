<?php
require('php/head.php');
?>
<link rel="stylesheet" href="css/form-basic.css">
<title>Mitglied bearbeiten</title>
</head>
<?php
require_once('db/database_connect.php');
require_once('php/menu.php');

?>

<form class="form-basic" action = "mitglied_bearbeiten_change.php" method = "post">
    <div class="form-title-row"> <h1>Zu bearbeitendes Mitglied Auswählen</h1></div>
    <div class="form-row">
        <label><span>Name</span> <select name = "mitglied_ID" >
                <?php
                $link=connect();

                $sql = "SELECT ID, vorname,nachname,spitzname FROM `mitglieder` where status_id not like 4";

                $rs=mysqli_query($link, $sql);
                while($row = mysqli_fetch_array($rs)) {
                    //pulls names for dropdown
                    if(is_null($row['spitzname'])){
                        $name=$row['vorname']." ".$row['nachname'];
                    }else{
                        $name=$row['vorname']." \"".$row['spitzname']."\" ".$row['nachname'];}
                    $ID=$row['ID'];
                    echo("<option value=\"$ID\"> $name</option>");
                }
                disconnect($link);
                ?>


            </select></label></div>
    <p><input type = "submit" value="Auswählen" class="submit_button"/></p>
</form>
</body>
</html>
