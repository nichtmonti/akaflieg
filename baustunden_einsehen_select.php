<?php
require('php/head.php');
?>
<link rel="stylesheet" href="css/form-basic.css" xmlns="http://www.w3.org/1999/html">
<title>Baustunden einsehen</title>
    </head>
<?php
require_once('db/database_connect.php');
require('php/menu.php');

?>

<form class="form-basic" action = "baustunden_einsehen.php" method = "post">
    <div class="form-title-row"> <h1>Arbeitszeit einsehen</h1></div>
    <div class="form-row">
        <label>
            <span>Name</span>
            <select name = "ID" >
                <?php
                $link=connect();

                $sql = "SELECT ID, vorname,nachname,spitzname FROM `mitglieder` where status_id not like 4";

                $rs=mysqli_query($link, $sql);
                while($row = mysqli_fetch_array($rs)) {
                    //pulls names for dropdown
                    if(is_null($row['spitzname'])){
                        $name=$row['vorname']." ".$row['nachname'];
                    }else{
                        $name=$row['vorname']." \"".$row['spitzname']."\" ".$row['nachname'];}
                    $ID=$row['ID'];
                    echo("<option value=\"$ID\"> $name</option>");
                }
                disconnect($link);
                ?>


             </select>
        </label>
    </div>
    <p><input class="submit_button" type = "submit" value="Auswählen" /></p>
            </form>
            </body>
            </html>
