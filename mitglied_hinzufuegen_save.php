<?php
require('php/head.php');
require_once ('php/custom_errors.php');
?>
<link rel="stylesheet" href="css/form-basic.css">
<title>Mitglied hinzufügen</title>
</head>
<?php
require_once('db/database_connect.php');
require('php/menu.php');
$vorname=$_POST["Vorname"];
$nachname=$_POST["Nachname"];
$spitzname=$_POST["Spitzname"];
$status_id=$_POST["status_id"];

$link=connect();

$stmt = mysqli_stmt_init($link);
//mitglied hinzufügen
$sql="Insert into mitglieder (Vorname,Spitzname,Nachname,Status_ID) Values(?,?,?,?)";
$stmt->prepare($sql);
$stmt->bind_param("sssi",$vorname,$spitzname,$nachname,$status_id);
$stmt->execute();

if ($stmt->errno > 0) {
    $error=$stmt->error;
    $stmt->close();
    sqlError(8191,$stmt->error);
}
//neuen Eintrag anzeigen

    $stmt->close();
    $mitglied_id=$link->insert_id;

    $stmt = mysqli_stmt_init($link);
    $sql = "SELECT Vorname,Spitzname,Nachname,Status FROM mitglieder LEFT JOIN status on mitglieder.Status_ID=status.ID WHERE mitglieder.ID LIKE ?";
    $stmt = mysqli_stmt_init($link);
    $stmt->prepare($sql);
    $stmt->bind_param("i", $mitglied_id);
    $stmt->execute();
if ($stmt->errno > 0) {
    $error=$stmt->error;
    $stmt->close();
    sqlError(8191,$stmt->error);
}
    $stmt->bind_result($vorname_neu,$spitzname_neu, $nachname_neu, $new_status);
    $stmt->fetch();

    $stmt->close();


    disconnect($link);
    ?>

<form class="form-basic" action = "mitglied_bearbeiten_select.php" method = "post" ">

<div class="form-title-row"> <h1>Änderungen erfolgreich gespeichert</h1></div>

<div class="form-row">
    <label>
        <span>Vorname</span>
        <input  value="<?php echo($vorname_neu); ?>"class="fixed" readonly />
</label>
</div>

<div class="form-row">
    <label>
        <span>Spitzname</span>
        <input  value="<?php echo($spitzname_neu); ?>"class="fixed" readonly />
    </label>
</div>

<div class="form-row">
    <label>
        <span>Nachname</span>
        <input  value="<?php echo($nachname_neu); ?>"class="fixed" readonly />
    </label>
</div>

<div class="form-row">
    <label>
        <span>Status</span>
        <input  value="<?php echo($new_status); ?>"class="fixed" readonly />
    </label>
</div>




<p><input type = "submit" value="Weiteres Mitglied Bearbeiten" class="submit_button" /> </p>


</body>
</html>
