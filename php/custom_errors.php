<?php
/**
 * Created by PhpStorm.
 * User: Sebastian
 * Date: 07.04.2016
 * Time: 18:54
 */

function validationError($errno, $errstr) {
    echo "<b>Error:</b> [$errno] $errstr<br>";
    die();
}

function sqlError($errno, $errstr) {
    echo "<b>Error:</b> [$errno] $errstr<br>";
    mysqli_close();
    die();
}