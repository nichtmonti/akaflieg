<?php
/**
 * Created by PhpStorm.
 * User: Sebastian
 * Date: 08.04.2016
 * Time: 17:30
 */

require_once('fpdf/fpdf.php');

class PDF extends CPDF
{
    function BaseTable($result)
    {
        //sets width for the cells
        $w = array(28,20,25,25,20,22,18,24,15);
        $height=5;
        $this->SetFillColor(210,210,210);

        $init = mysqli_fetch_array($result, MYSQLI_ASSOC);
// Header
        $w_count=0; //To match header width with line width
        foreach ($init as $key => $value) {
            $this->Cell($w[$w_count++], $height, utf8_decode($key), 1);
        }
        $this->Ln();

        $row = $init;
        $fill=false;
        do {
            $this->Cell($w[0], $height, utf8_decode($row['Vorname']), 'LR',0,'L',$fill);
            $this->Cell($w[1], $height, utf8_decode($row['Spitzname']), 'LR',0,'L',$fill);
            $this->Cell($w[2], $height, utf8_decode($row['Nachname']), 'LR',0,'L',$fill);
            $this->Cell($w[3], $height, utf8_decode($row['Gesamtarbeitszeit']), 'LR',0,'L',$fill);
            $this->Cell($w[4], $height, utf8_decode($row['Flugberechtigt']), 'LR',0,'L',$fill);
            $this->Cell($w[5], $height, utf8_decode($row['Stunden/Monat']), 'LR',0,'L',$fill);
            $this->Cell($w[6], $height, utf8_decode($row['Überschuss']), 'LR',0,'L',$fill);
            $this->Cell($w[7], $height, utf8_decode($row['Minimum erreicht']), 'LR',0,'L',$fill);
            $this->Cell($w[8], $height, utf8_decode($row['Aktiv?']), 'LR',0,'L',$fill);


            $fill=!$fill;
            $this->Ln();
        }
        while($row = mysqli_fetch_array($result,MYSQLI_ASSOC));

        // Closing line
        $this->Cell(array_sum($w),0,'','T');

    }
}

function Overview_Table($result){
    $pdf=new PDF('P','mm');
    $pdf->SetFont('Arial','',8);
    $pdf->AddPage();
    $pdf->BaseTable($result);
    return $pdf;
}

