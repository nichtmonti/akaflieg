<?php
require_once('phpgraphlib.php');

require_once('db/database_connect.php');


//get data from database
$link=connect();
$sql = "SELECT * FROM `gesamtarbeitszeit_pro_mitglied`";
$result=mysqli_query($link,$sql);
disconnect($link);

$graph_data = array();
//create array
while($res=mysqli_fetch_array($result,MYSQLI_ASSOC)){


    $graph_data[$res['Name']]=number_format((float)$res['Gesamtarbeitszeit']/60, 2, '.', '');//Graph in Stunden

    echo("");

}



$graph = new PHPGraphLib(900,600,'graphen/gesamtbaustunden.png');
$data = $graph_data;
$graph->addData($data);
$graph->setBarColor('navy');
$graph->setBarOutlineColor("black");
$graph->setupXAxis(20, 'blue');
$graph->setGrid(true);
$graph->setTitle(utf8_decode('Übersicht Gesamtarbeitszeit in Stunden'));
$graph->setTitleColor('blue');
$graph->setGridColor('153,204,255');
$graph->setDataValues(false);
$graph->setGoalLine(40, "red", "solid");
$graph->setDataFormat("");


$graph->setDataValueColor('navy');



$graph->createGraph();

?>