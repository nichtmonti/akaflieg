<?php
require_once('phpgraphlib.php');

require_once('db/database_connect.php');

//get data from database
$link=connect();
$sql = "SELECT * FROM `gesamtarbeitszeit_pro_mitglied`";
$result=mysqli_query($link,$sql);
disconnect($link);

$graph_data = array();
//create array
while($res=mysqli_fetch_array($result,MYSQLI_ASSOC)){

    $graph_data[$res['Name']]=number_format((float)$res['Relativbaustunden']/60, 2, '.', '');//Graph in Stunden

}


$graph = new PHPGraphLib(900,600,'graphen/relativbaustunden.png');
$data = $graph_data;
$graph->addData($data);
$graph->setBarColor('navy');
$graph->setBarOutlineColor("black");
$graph->setupXAxis(20, 'blue');

$graph->setTitle(utf8_decode('Übersicht Relativbaustunden'));
$graph->setTitleColor('blue');
$graph->setGridColor('153,204,255');
$graph->setDataValues(false);
$graph->setDataValueColor('navy');
$graph->createGraph();



?>