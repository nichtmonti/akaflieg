-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 08. Apr 2016 um 18:26
-- Server-Version: 5.6.26
-- PHP-Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `baustunden`
--
CREATE DATABASE IF NOT EXISTS `baustunden` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `baustunden`;

DELIMITER $$
--
-- Funktionen
--
DROP FUNCTION IF EXISTS `abs_minimum_erreicht`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `abs_minimum_erreicht`(`arbeitszeit` INT) RETURNS varchar(4) CHARSET utf8 COLLATE utf8_bin
    NO SQL
IF (arbeitszeit>=2400) THEN RETURN "ja";
ELSE RETURN "nein";
END IF$$

DROP FUNCTION IF EXISTS `bool_to_string`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `bool_to_string`(`bool` BOOLEAN) RETURNS varchar(4) CHARSET utf8 COLLATE utf8_bin
    NO SQL
IF (bool) THEN RETURN "ja";
ELSE RETURN "nein";
END IF$$

DROP FUNCTION IF EXISTS `flugberechtigt`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `flugberechtigt`(`status_id` INT, `arbeitszeit` INT) RETURNS varchar(4) CHARSET utf8 COLLATE utf8_bin
    READS SQL DATA
BEGIN
DECLARE minimum int(10);
SELECT `min_hr/monat` from status where status.ID LIKE status_id INTO minimum;

IF (minimum IS NULL) 
THEN RETURN 'nein';


ELSEIF (status_id IN (2,3,5,6)) THEN

	IF (minimum*60*(Month(NOW())+2) < arbeitszeit AND 40 < arbeitszeit) THEN RETURN 'ja';
    ELSE RETURN 'nein';
    END IF;
ELSE RETURN 'nein';    
   END IF;
END$$

DROP FUNCTION IF EXISTS `format_time`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `format_time`(`time` INT) RETURNS text CHARSET utf8 COLLATE utf8_bin
    NO SQL
IF (time is NULL) THEN RETURN "0:00";
ELSEIF (time >=0) THEN
RETURN Concat_ws(":",FLOOR(time/60),LPAD(time%60,2,0));

ELSE RETURN Concat("-",Concat_ws(":",FLOOR(-time/60),LPAD(-time%60,2,0)));
END IF$$

DROP FUNCTION IF EXISTS `min_hrs`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `min_hrs`(`status_id` INT) RETURNS decimal(4,2)
    READS SQL DATA
BEGIN SET @hrs =(SELECT `min_hr/monat` from status WHERE ID Like status_id);
IF(@hrs is NULL) THEN SET @hrs=0;
   END IF;
   RETURN @hrs;
END$$

DROP FUNCTION IF EXISTS `stunden-Pflichtstunden`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `stunden-Pflichtstunden`(`arbeitszeit` INT, `status_id` INT) RETURNS varchar(20) CHARSET utf8 COLLATE utf8_bin
    NO SQL
    COMMENT 'Arbeitszeit in Minuten'
BEGIN
SET @minimum = min_hrs(status_id);

IF(@minimum is NULL) THEN SET @minimum=0;

ELSEIF(arbeitszeit is NULL)THEN SET arbeitszeit=0;

END IF;

RETURN format_time(arbeitszeit-
                   (@minimum*
                    60*
                    (Month(NOW())+2)
                   ));



END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `eintrag`
--

DROP TABLE IF EXISTS `eintrag`;
CREATE TABLE IF NOT EXISTS `eintrag` (
  `ID` int(11) NOT NULL,
  `Datum` date NOT NULL,
  `Beschreibung` varchar(65) COLLATE utf8_bin NOT NULL,
  `Mitglied_ID` int(11) NOT NULL,
  `Projekt_ID` int(11) NOT NULL,
  `Arbeitszeit` int(11) NOT NULL COMMENT 'Arbeitszeit in Minuten'
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Daten für Tabelle `eintrag`
--

INSERT INTO `eintrag` (`ID`, `Datum`, `Beschreibung`, `Mitglied_ID`, `Projekt_ID`, `Arbeitszeit`) VALUES
(1, '2016-03-14', 'Aufrüsten', 1, 1, 20000),
(3, '0000-00-00', 'adasdasd', 1, 1, 60000),
(4, '0000-00-00', 'asdasd234', 1, 1, 110000),
(7, '0000-00-00', 'asdasdqwe2', 1, 1, 50700),
(8, '0000-00-00', 'adasdasd', 3, 1, 20000),
(9, '0000-00-00', 'asdwq', 1, 1, 600),
(10, '0000-00-00', 'asd', 1, 1, 300),
(11, '2016-03-09', 'asd', 1, 1, 500),
(12, '2016-03-15', 'asdasdq', 1, 1, 1300),
(13, '2016-03-02', 'as', 1, 1, 500),
(14, '2016-03-01', 'adq', 1, 1, 320),
(15, '2016-03-08', 'adasd', 1, 1, 121),
(16, '2016-03-09', 'sfdf', 1, 1, 124),
(17, '2016-03-08', 'dsadsds', 1, 1, 121),
(18, '2016-03-01', 'asd', 1, 1, 184),
(19, '2016-03-09', 'asd', 1, 1, 122),
(20, '2016-03-02', 'asd', 1, 1, 122),
(21, '2016-03-01', 'asd', 1, 1, 63),
(22, '2016-03-02', 'asdasd', 1, 1, 121),
(23, '2016-03-17', 'asdq', 1, 1, 61),
(24, '2016-03-10', 'asd', 1, 1, 123),
(28, '2016-03-01', 'q', 1, 1, 61),
(29, '2016-03-23', 'asd', 1, 1, 123),
(30, '2016-03-23', 'asd', 1, 1, 123),
(31, '2016-03-09', 'asdyr123', 1, 1, 184),
(32, '2016-03-15', 'as', 1, 1, 62),
(33, '2016-03-15', 'as', 1, 1, 62),
(34, '2016-03-15', 'as', 1, 1, 62),
(35, '2016-03-15', 'as', 1, 1, 62),
(36, '2016-03-15', 'as', 1, 1, 62),
(37, '2016-03-11', 'asdads', 1, 1, 61),
(38, '2016-03-24', '; drop \\"', 1, 1, 61),
(39, '2016-03-17', 'asd', 1, 1, 61),
(40, '2016-03-15', 'as', 1, 1, 61),
(41, '2016-03-01', 'asd', 1, 1, 61),
(42, '2016-03-01', 'asd', 1, 1, 61),
(43, '2016-03-09', 'asdq', 2, 1, 1381),
(44, '2016-03-16', 'adaqw', 1, 1, 61),
(45, '2016-03-02', 'test', 1, 1, 123),
(46, '2016-03-02', 'test', 1, 1, 123),
(47, '2016-03-02', 'test', 1, 1, 123),
(48, '2016-03-02', 'test', 1, 1, 123),
(49, '2016-03-08', 'tea', 1, 1, 123),
(50, '2016-03-01', 'ads', 1, 1, 121),
(51, '2016-03-01', 'ads', 1, 1, 121),
(52, '2016-03-02', 'asd', 1, 1, 62),
(53, '2016-03-02', 'asd', 1, 1, 62),
(54, '2016-03-02', 'asd', 1, 1, 62),
(55, '2016-03-01', 'asdq', 1, 1, 61),
(56, '2016-03-01', 'asdq', 1, 1, 61),
(57, '2016-03-01', 'asdq', 1, 1, 61),
(58, '2016-03-01', 'asdq', 1, 1, 61),
(59, '2016-03-01', 'asdq', 1, 1, 61),
(60, '2016-03-04', 'asdq', 1, 1, 61),
(61, '2016-03-04', 'asdq', 1, 1, 61),
(62, '2016-03-16', 'adsqwe', 1, 1, 61),
(63, '2016-03-01', 'asdasd', 1, 1, 61),
(64, '2016-03-01', 'asdasd', 1, 1, 61),
(65, '2016-03-09', 'sdqwe', 1, 1, 62),
(66, '2016-03-09', 'asdqwe', 1, 1, 61),
(67, '2016-03-09', 'asdqwe', 1, 1, 61),
(68, '2016-03-09', 'asdqwe', 1, 1, 61),
(69, '2016-03-09', 'asdqwe', 1, 1, 61),
(70, '2016-03-09', 'asdqwe', 1, 1, 61),
(71, '2016-03-02', 'qwdas', 1, 1, -118),
(72, '2016-03-16', 'sf', 1, 1, 61),
(73, '2016-03-01', 'asd', 1, 1, 61),
(74, '2016-03-23', '123', 1, 1, 62),
(75, '2016-03-23', '123', 1, 1, 62),
(76, '2016-03-23', '123', 1, 1, 62),
(77, '2016-03-23', '123', 1, 1, 62),
(78, '2016-03-23', '123', 1, 1, 62),
(79, '2016-03-23', '123', 1, 1, 62),
(80, '2016-03-23', '123', 1, 1, 62),
(81, '2016-04-07', 'asdasd234', 1, 1, 62),
(82, '2016-04-06', 'asd', 7, 1, 61),
(83, '2016-04-04', 'asd', 8, 1, 62);

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `full_view_by_id`
--
DROP VIEW IF EXISTS `full_view_by_id`;
CREATE TABLE IF NOT EXISTS `full_view_by_id` (
`Eintrag_ID` int(11)
,`mitglied_id` int(11)
,`Datum` date
,`Name` varchar(41)
,`Mitarbeiter` text
,`Projekt` varchar(20)
,`Beschreibung` varchar(65)
,`Arbeitszeit` text
);

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `gesamtarbeitszeit_pro_mitglied`
--
DROP VIEW IF EXISTS `gesamtarbeitszeit_pro_mitglied`;
CREATE TABLE IF NOT EXISTS `gesamtarbeitszeit_pro_mitglied` (
`ID` int(11)
,`Gesamtarbeitszeit` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mitglieder`
--

DROP TABLE IF EXISTS `mitglieder`;
CREATE TABLE IF NOT EXISTS `mitglieder` (
  `ID` int(11) NOT NULL,
  `Vorname` varchar(20) COLLATE utf8_bin NOT NULL,
  `Nachname` varchar(20) COLLATE utf8_bin NOT NULL,
  `Spitzname` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `Status_ID` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Daten für Tabelle `mitglieder`
--

INSERT INTO `mitglieder` (`ID`, `Vorname`, `Nachname`, `Spitzname`, `Status_ID`) VALUES
(1, 'Sebastian', 'Clermont', NULL, 2),
(2, 'Test1', 'Bla', NULL, 1),
(3, 'Test2', 'nachtest', NULL, 3),
(4, 'test3', 'nachtest32', NULL, 1),
(5, 'ein', 'ehemaliger', NULL, 4),
(6, 'Mitglied', 'Neues1', NULL, 4),
(7, 'Mitglied', 'Neues', NULL, 1),
(8, 'qewasd', 'asddsa', NULL, 3),
(9, 'neu', 'neu', NULL, 4),
(10, 'Arbeiter', 'fauler', NULL, 3);

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `pdf`
--
DROP VIEW IF EXISTS `pdf`;
CREATE TABLE IF NOT EXISTS `pdf` (
`Vorname` varchar(20)
,`Nachname` varchar(20)
,`Gesamtarbeitszeit` text
,`Flugberechtigt` varchar(4)
,`geforderte Stunden` decimal(4,2)
,`Überschuss` varchar(20)
,`Minimum erreicht` varchar(4)
,`Aktiv?` varchar(4)
);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `projekt`
--

DROP TABLE IF EXISTS `projekt`;
CREATE TABLE IF NOT EXISTS `projekt` (
  `ID` int(11) NOT NULL,
  `Name` varchar(20) COLLATE utf8_bin NOT NULL,
  `Beschreibung` text COLLATE utf8_bin
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Daten für Tabelle `projekt`
--

INSERT INTO `projekt` (`ID`, `Name`, `Beschreibung`) VALUES
(1, 'D-43', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `projekt_helfer`
--

DROP TABLE IF EXISTS `projekt_helfer`;
CREATE TABLE IF NOT EXISTS `projekt_helfer` (
  `ID` int(11) NOT NULL,
  `eintrag_ID` int(11) NOT NULL,
  `mitglieder_ID` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Daten für Tabelle `projekt_helfer`
--

INSERT INTO `projekt_helfer` (`ID`, `eintrag_ID`, `mitglieder_ID`) VALUES
(1, 1, 2),
(3, 12, 1),
(4, 7, 2),
(7, 11, 3),
(8, 44, 2),
(9, 65, 3),
(10, 67, 2),
(11, 67, 3),
(12, 69, 2),
(13, 69, 3),
(14, 70, 2),
(15, 70, 3),
(16, 71, 3);

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `projekt_helfer_view`
--
DROP VIEW IF EXISTS `projekt_helfer_view`;
CREATE TABLE IF NOT EXISTS `projekt_helfer_view` (
`Eintrag_ID` int(11)
,`Name` text
,`Beschreibung` varchar(65)
);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE IF NOT EXISTS `status` (
  `ID` int(11) NOT NULL,
  `Status` varchar(30) CHARACTER SET utf8 NOT NULL,
  `min_hr/monat` decimal(4,2) DEFAULT NULL,
  `Aktiv` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Daten für Tabelle `status`
--

INSERT INTO `status` (`ID`, `Status`, `min_hr/monat`, `Aktiv`) VALUES
(1, 'Interessent', NULL, 0),
(2, 'Anwärter', '12.50', 0),
(3, 'Aktiv ohne Flugschein', '16.00', 1),
(4, 'Ehemalig', NULL, 0),
(5, 'Aktiv mit Flugschein', '16.70', 1),
(6, 'Aktiv + Wettbewerbe', '20.80', 1),
(7, 'Urlaub', NULL, 1);

-- --------------------------------------------------------

--
-- Struktur des Views `full_view_by_id`
--
DROP TABLE IF EXISTS `full_view_by_id`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `full_view_by_id` AS select `eintrag`.`ID` AS `Eintrag_ID`,`mitglieder`.`ID` AS `mitglied_id`,`eintrag`.`Datum` AS `Datum`,concat_ws(' ',`mitglieder`.`Vorname`,`mitglieder`.`Nachname`) AS `Name`,`hview`.`Name` AS `Mitarbeiter`,`projekt`.`Name` AS `Projekt`,`eintrag`.`Beschreibung` AS `Beschreibung`,`format_time`(`eintrag`.`Arbeitszeit`) AS `Arbeitszeit` from (((`eintrag` join `mitglieder` on((`eintrag`.`Mitglied_ID` = `mitglieder`.`ID`))) join `projekt` on((`eintrag`.`Projekt_ID` = `projekt`.`ID`))) left join `projekt_helfer_view` `hview` on((`eintrag`.`ID` = `hview`.`Eintrag_ID`))) order by `eintrag`.`ID`;

-- --------------------------------------------------------

--
-- Struktur des Views `gesamtarbeitszeit_pro_mitglied`
--
DROP TABLE IF EXISTS `gesamtarbeitszeit_pro_mitglied`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `gesamtarbeitszeit_pro_mitglied` AS select `mitglieder`.`ID` AS `ID`,sum(`eintrag`.`Arbeitszeit`) AS `Gesamtarbeitszeit` from (`mitglieder` left join `eintrag` on((`mitglieder`.`ID` = `eintrag`.`Mitglied_ID`))) where (`mitglieder`.`Status_ID` <> 4) group by `mitglieder`.`ID`;

-- --------------------------------------------------------

--
-- Struktur des Views `pdf`
--
DROP TABLE IF EXISTS `pdf`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pdf` AS select `mitglieder`.`Vorname` AS `Vorname`,`mitglieder`.`Nachname` AS `Nachname`,`format_time`(`gesamtarbeitszeit_pro_mitglied`.`Gesamtarbeitszeit`) AS `Gesamtarbeitszeit`,`Flugberechtigt`(`mitglieder`.`Status_ID`,`gesamtarbeitszeit_pro_mitglied`.`Gesamtarbeitszeit`) AS `Flugberechtigt`,`min_hrs`(`mitglieder`.`Status_ID`) AS `geforderte Stunden`,`stunden-Pflichtstunden`(`gesamtarbeitszeit_pro_mitglied`.`Gesamtarbeitszeit`,`mitglieder`.`Status_ID`) AS `Überschuss`,`abs_minimum_erreicht`(`gesamtarbeitszeit_pro_mitglied`.`Gesamtarbeitszeit`) AS `Minimum erreicht`,`bool_to_string`(`status`.`Aktiv`) AS `Aktiv?` from (((`gesamtarbeitszeit_pro_mitglied` join `mitglieder` on((`gesamtarbeitszeit_pro_mitglied`.`ID` = `mitglieder`.`ID`))) left join `eintrag` on((`mitglieder`.`ID` = `eintrag`.`Mitglied_ID`))) join `status` on((`mitglieder`.`Status_ID` = `status`.`ID`))) where (`mitglieder`.`Status_ID` <> 4) group by `mitglieder`.`ID`;

-- --------------------------------------------------------

--
-- Struktur des Views `projekt_helfer_view`
--
DROP TABLE IF EXISTS `projekt_helfer_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `projekt_helfer_view` AS select `eintrag`.`ID` AS `Eintrag_ID`,group_concat(concat_ws(' ',`mitglieder`.`Vorname`,`mitglieder`.`Nachname`) separator ', ') AS `Name`,`eintrag`.`Beschreibung` AS `Beschreibung` from ((`projekt_helfer` join `mitglieder` on((`projekt_helfer`.`mitglieder_ID` = `mitglieder`.`ID`))) join `eintrag` on((`projekt_helfer`.`eintrag_ID` = `eintrag`.`ID`))) group by `eintrag`.`ID`;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `eintrag`
--
ALTER TABLE `eintrag`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Mitglied_ID` (`Mitglied_ID`),
  ADD KEY `Projekt_ID` (`Projekt_ID`),
  ADD KEY `Mitglied_ID_2` (`Mitglied_ID`),
  ADD KEY `Mitglied_ID_3` (`Mitglied_ID`);

--
-- Indizes für die Tabelle `mitglieder`
--
ALTER TABLE `mitglieder`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Status_ID` (`Status_ID`);

--
-- Indizes für die Tabelle `projekt`
--
ALTER TABLE `projekt`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `projekt_helfer`
--
ALTER TABLE `projekt_helfer`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `eintrag_ID` (`eintrag_ID`),
  ADD KEY `Helfer_ID` (`mitglieder_ID`);

--
-- Indizes für die Tabelle `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Status` (`Status`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `eintrag`
--
ALTER TABLE `eintrag`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT für Tabelle `mitglieder`
--
ALTER TABLE `mitglieder`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `projekt`
--
ALTER TABLE `projekt`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `projekt_helfer`
--
ALTER TABLE `projekt_helfer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT für Tabelle `status`
--
ALTER TABLE `status`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `eintrag`
--
ALTER TABLE `eintrag`
  ADD CONSTRAINT `eintrag_ibfk_1` FOREIGN KEY (`Mitglied_ID`) REFERENCES `mitglieder` (`ID`),
  ADD CONSTRAINT `eintrag_ibfk_2` FOREIGN KEY (`Projekt_ID`) REFERENCES `projekt` (`ID`);

--
-- Constraints der Tabelle `mitglieder`
--
ALTER TABLE `mitglieder`
  ADD CONSTRAINT `mitglieder_ibfk_1` FOREIGN KEY (`Status_ID`) REFERENCES `status` (`ID`);

--
-- Constraints der Tabelle `projekt_helfer`
--
ALTER TABLE `projekt_helfer`
  ADD CONSTRAINT `projekt_helfer_ibfk_1` FOREIGN KEY (`eintrag_ID`) REFERENCES `eintrag` (`ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `projekt_helfer_ibfk_2` FOREIGN KEY (`mitglieder_ID`) REFERENCES `mitglieder` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
