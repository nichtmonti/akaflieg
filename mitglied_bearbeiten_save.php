<?php
require('php/head.php');
require_once('php/make_table.php');
require_once ('php/custom_errors.php');
?>
<link rel="stylesheet" href="css/form-basic.css">
<title>Mitglied bearbeiten</title>
    </head>
<?php

require_once('db/database_connect.php');
require('php/menu.php');



$vorname_alt=$_POST["old_vorname"];
$spitzname_alt=$_POST["old_spitzname"];
$nachname_alt=$_POST["old_nachname"];
$status_id_alt=$_POST["old_status_id"];


$vorname=$_POST["vorname"];
$spitzname=$_POST["spitzname"];
$nachname=$_POST["nachname"];
$status_id=$_POST["status_id"];

$mitglied_id=$_POST["mitglied_id"];
//echo($status_id_alt);

if($vorname==$vorname_alt&&$nachname==$nachname_alt&&$status_id==$status_id_alt&&$spitzname==$spitzname_alt){
   validationError(256,"keine Änderungen eingegeben");
}

    $link = connect();
    $stmt = mysqli_stmt_init($link);
    $sql = "Update mitglieder SET Vorname=?, nachname=?, status_id=? WHERE mitglieder.id=?";
    $stmt->prepare($sql);
    $stmt->bind_param("ssii", $vorname, $nachname, $status_id, $mitglied_id);
    $stmt->execute();

    if ($stmt->errno > 0) {
        $stmt->close();
        sqlError(8191,$stmt->error);
    }

        $stmt->close();
        disconnect($link);
        $link=connect();

        //show result
        $stmt = mysqli_stmt_init($link);
        $sql = "SELECT Vorname,Nachname,Status FROM mitglieder LEFT JOIN status on mitglieder.Status_ID=status.ID WHERE mitglieder.ID LIKE ?";
        $stmt = mysqli_stmt_init($link);
        $stmt->prepare($sql);
        $stmt->bind_param("i", $mitglied_id);
        $stmt->execute();
        $stmt->bind_result($vorname_neu, $nachname_neu, $new_status);
        $stmt->fetch();
        $stmt->close();


        disconnect($link);


   ?>
<form class="form-basic" action = "mitglied_bearbeiten_select.php" method = "post" ">

<div class="form-title-row"> <h1>Änderungen erfolgreich gespeichert</h1></div>

<div class="form-row">
    <label>
        <span>Vorname</span>
        <input  value="<?php echo($vorname_neu); ?>"class="fixed" readonly />
    </label>
</div>

<div class="form-row">
    <label>
        <span>Spitzname</span>
        <input  value="<?php echo($spitzname); ?>"class="fixed" readonly />
    </label>
</div>

<div class="form-row">
    <label>
        <span>Nachname</span>
        <input  value="<?php echo($nachname_neu); ?>"class="fixed" readonly />
    </label>
</div>

<div class="form-row">
    <label>
        <span>Status</span>
        <input  value="<?php echo($new_status); ?>"class="fixed" readonly />
    </label>
</div>




<p><input type = "submit" value="Weiteres Mitglied Bearbeiten" class="submit_button" /> </p>


</body>
</html>
