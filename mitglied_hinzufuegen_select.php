<?php
require('php/head.php');
?>
<link rel="stylesheet" href="css/form-basic.css" xmlns="http://www.w3.org/1999/html">
<title>Mitglied hinzufügen</title>
</head>
<?php
require_once('db/database_connect.php');
require('php/menu.php');

?>

<form class="form-basic" action = "mitglied_hinzufuegen_save.php" method = "post" ">

<div class="form-title-row"> <h1>Mitglied Hinzufügen</h1></div>

<div class="form-row">
    <label>
        <span>Vorname</span>
        <input  name="Vorname" />
</label>
</div>
<div class="form-row">
    <label>
        <span>Spitzname</span>
        <input  name="Spitzname" />
    </label>
</div>


<div class="form-row">
    <label>
        <span>Nachname</span>
        <input  name="Nachname" />
    </label>
</div>


<div class="form-row">
    <label>
        <span>Status</span> <select name="status_id" >
            <?php
            $link=connect();

            $sql = "SELECT ID,Status FROM status ";

            $rs=mysqli_query($link, $sql);
            while($row = mysqli_fetch_array($rs)) { //pulls names for dropdown
                $status_name=$row['Status'];
                $status_ID=$row['ID'];
                if($current_status_id==$status_ID){
                    echo("<option selected=\"selected\" value=\"$status_ID\"> $status_name</option>");
                }
                else{ echo("<option value=\"$status_ID\"> $status_name</option>");}
            }
            disconnect($link);
            ?>


    </label>
    </select>
</div>



<p><input type = "submit" value="Mitglied hinzufügen" class="submit_button" /> </p>
</form>
</body>
</html>


