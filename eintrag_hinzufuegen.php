<?php
require('php/head.php');
require_once ('php/custom_errors.php');
?>
<title>Baustunden eintragen</title>
</head>
<?php
$fail=false;
function all_numeric($thearray){
    $all_numeric = true;
    foreach ($thearray as $key) {
        if (!(is_numeric($key))) {
            $all_numeric = false;
            break;
        }
    }
    return $all_numeric;
}

function display_result($link, $eintrag_id){
    $sql= "SELECT Datum, Mitarbeiter, Beschreibung, Projekt,Arbeitszeit FROM full_view_by_id WHERE eintrag_ID Like $eintrag_id";
    echo("<h2>Eintrag erfolgreich erstellt</h2>");
    table(mysqli_query($link, $sql));
    disconnect($link);

}

    require('php/menu.php');

if(!$_POST) {
    validationError(254,"Diese Seite nicht direkt aufrufen");
}
    require('db/database_connect.php');
    require('php/make_table.php');

//get values from form
    $mitglied_ID = $_POST["mitglied_ID"];
    $datum = $_POST["datum"];
    $projekt_ID = $_POST["projekt_ID"];
    $beschreibung = $_POST["beschreibung"];

    $stunden = $_POST["stunden"];
    $minuten = $_POST["minuten"];

if(!isset($_POST["helfer_ID"])){
    $solo=true;
}
else {

    $mithelfer_IDs = $_POST["helfer_ID"];
}

 //validate data
    if(!(is_numeric($stunden)&&$stunden>=0&&is_numeric($minuten)&&$minuten>0&&is_numeric($mitglied_ID)&&($solo||all_numeric($mithelfer_IDs)))) {
     validationError(254,"Arbeitszeit muss positive Zahl sein");
    }
    else if(!preg_match("/^\d{4}-\d{2}-\d{2}/",$datum)) {
    validationError(254, "Ungültiges Datumsformat");
}


    //insert data in eintrag


    $link = connect();
    $stmt = mysqli_stmt_init($link);
    $sql = "INSERT INTO eintrag(Datum,Beschreibung,Mitglied_ID,Projekt_ID,Arbeitszeit) VALUES(?,?,?,?,?)";
    $stmt->prepare($sql);
    $zeit = (int)$stunden * 60 + (int)$minuten;
    $clean_beschreibung = $link->real_escape_string($beschreibung);
    $stmt->bind_param("ssiii", $datum, $clean_beschreibung, $mitglied_ID, $projekt_ID, $zeit);
    echo $stmt->error;
    $stmt->execute();

    $eintrag_id = mysqli_insert_id($link);

    if ($stmt->errno != 0) {
        $error=$stmt->error;
        $stmt->close();
        sqlError(8191,$error);
    }

    $stmt->close();

    if (!$solo) {

    //insert mithelfer data


    //values are checked to be numeric earlier
    $sql = "INSERT INTO `projekt_helfer` (`eintrag_ID`, `mitglieder_ID`) VALUES";

    foreach ($mithelfer_IDs as $h_id) {
        $sql .= "($eintrag_id,$h_id),";
    }
    $sql = substr($sql, 0, -1); //removes last comma
    $sql .= ";";

    mysqli_query($link, $sql);


    if (mysqli_errno($link) != 0) {
        $error=$stmt->error;
        $stmt->close();
        sqlError(8191,$error);
    }
}
    //display inserted data

display_result($link,$eintrag_id);





?>
</body>
</html>
