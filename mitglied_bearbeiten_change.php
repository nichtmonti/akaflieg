<?php
require('php/head.php');
require_once('php/make_table.php');
?>
<link rel="stylesheet" href="css/form-basic.css">
<title>Mitglied Bearbeiten</title>
</head>
<?php
require_once('db/database_connect.php');
require('php/menu.php');

$mitglied_ID = $_POST["mitglied_ID"];

$link = connect();

$sql = "Select Vorname,Nachname,Spitzname,status.Status,status.ID from mitglieder LEFT JOIN status on mitglieder.Status_ID=status.id WHERE mitglieder.ID LIKE ?";
$stmt = mysqli_stmt_init($link);
$stmt->prepare($sql);

$stmt->bind_param("i",$mitglied_ID);

$stmt->execute();

$stmt->bind_result($vorname,$nachname,$spitzname,$current_status,$current_status_id);
$stmt->fetch();
$stmt->close();
disconnect($link);


?>

<form class="form-basic" action = "mitglied_bearbeiten_save.php" method = "post" ">

<div class="form-title-row"> <h1>Mitglied Bearbeiten</h1></div>
<input type="hidden" name="mitglied_id" value="<?php echo($mitglied_ID);?>"/>
<input type="hidden" name="old_spitzname" value="<?php echo($spitzname);?>"/>
<input type="hidden" name="old_vorname" value="<?php echo($vorname);?>"/>
<input type="hidden" name="old_nachname" value="<?php echo($nachname);?>"/>
<input type="hidden" name="old_status_id" value="<?php echo($current_status_id);?>"/>
<div class="form-row">
    <label>
        <span>Vorname</span>
        <input name = "vorname" value="<?php echo($vorname); ?>" class="field-long" />
    </label>
</div>
<div class="form-row">
    <label>
        <span>Spitzname</span>
        <input name = "spitzname" value="<?php echo($spitzname); ?>" class="field-long" />
    </label>
</div>
<div class="form-row">
    <label>
        <span>Nachname</span>
        <input name = "nachname" value="<?php echo($nachname); ?>" class="field-long" />
    </label>
</div>



<div class="form-row">
        <label>
            <span>Status</span> <select name="status_id" >
                <?php
                $link=connect();

                $sql = "SELECT ID,Status FROM status ";

                $rs=mysqli_query($link, $sql);
                while($row = mysqli_fetch_array($rs)) { //pulls names for dropdown
                    $status_name=$row['Status'];
                    $status_ID=$row['ID'];
                    if($current_status_id==$status_ID){
                        echo("<option selected=\"selected\" value=\"$status_ID\"> $status_name</option>");
                    }
                    else{ echo("<option value=\"$status_ID\"> $status_name</option>");}
                }
                disconnect($link);
                ?>


</label>
</div>


<p><input type = "submit" value="Änderungen Speichern" class="submit_button" /> <input class="submit_button" type = "reset"  /></p>

</body>
</html>

