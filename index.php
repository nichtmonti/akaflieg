<?php

require('php/head.php');
?>
<script src="js/jquery.js" xmlns="http://www.w3.org/1999/html"></script>
<script src="js/jquery_ui.js"></script>
<link rel="stylesheet" href="css/datepicker.css">
<link rel="stylesheet" href="css/form-basic.css">
<link rel="stylesheet" href="css/chosen.css">
<title>Baustunden eintragen</title>
<script>
    $(function() {
        $( "#datepicker" ).datepicker(
            {
                dateFormat: "yy-mm-dd",
                 minDate: "-2M", maxDate: +0
            }
        );

    });

</script>
</head>
<?php
require('db/database_connect.php');
require('php/menu.php');
?>

<form class="form-basic" action = "eintrag_hinzufuegen.php" method = "post" ">

<div class="form-title-row"> <h1>Neuen Eintrag erstellen</h1></div>

    <div class="form-row">
    <label>
        <span>Name</span>
        <select name = "mitglied_ID">
            <?php
            $link=connect();

            $sql = "SELECT ID, vorname,nachname,spitzname FROM `mitglieder` where status_id not like 4";

            $rs=mysqli_query($link, $sql);
            while($row = mysqli_fetch_array($rs)) { //pulls names for dropdown
                if(is_null($row['spitzname'])){
                    $name=$row['vorname']." ".$row['nachname'];
                }else{
                $name=$row['vorname']." \"".$row['spitzname']."\" ".$row['nachname'];}
                $ID=$row['ID'];
                echo("<option value=\"$ID\"> $name</option>");
            }
            disconnect($link);
            ?>
     </select></label></div>

    <div class="form-row">
    <label>
            <span>Datum</span>
        <input name = "datum" id="datejs" placeholder="Format: YYYY-MM-DD"  />
        </div>

    <div class="form-row">
    <label>
        <span>Projekt</span> <select name = "projekt_ID"  >
            <?php
            $link=connect();
            $sql = "SELECT ID, Name FROM `projekt`";
            $rs=mysqli_query($link, $sql);

            while($row = mysqli_fetch_array($rs)) { //pulls names for dropdown
                $name=$row['Name'];
                $ID=$row['ID'];
                echo("<option value=\"$ID\"> $name</option>");
            }
            disconnect($link);
            ?>
        </select></label></div>

    <div class="form-row">
   <label>
       <span>Beschreibung</span>
       <input name = "beschreibung" class="field-long" />
       </label></div>


    <div class="form-row">
     <label>
       <span>Mithelfer</span>
       <select name = "helfer_ID[]"  class="chosen-select" multiple data-placeholder="Mithelfer auswählen" >

           <?php
           $link=connect();

           $sql = "SELECT ID, vorname,nachname,spitzname FROM `mitglieder` where status_id not like 4";

           $rs=mysqli_query($link, $sql);
           while($row = mysqli_fetch_array($rs)) {
               //pulls names for dropdown
               if(is_null($row['spitzname'])){
                   $name=$row['vorname']." ".$row['nachname'];
               }else{
                   $name=$row['vorname']." \"".$row['spitzname']."\" ".$row['nachname'];}
               $ID=$row['ID'];
               echo("<option value=\"$ID\"> $name</option>");
           }
           disconnect($link);
           ?>

           //js for multiselect
           <script src="js/chosen.jquery.js" type="text/javascript"></script>

           <script type="text/javascript">
               $(".chosen-select").chosen()
           </script>


    </select> </label></div>


    <div class="form-row">
    <label><span>Arbeitszeit</span> <input name = "stunden" type="number" class="field-divided" placeholder="Stunden" required/> <input name="minuten" type="number"   class="field-divided" placeholder="Minuten" required />
    </label></div>

    <p><input class="submit_button" type = "submit" value="Eintrag erstellen"/>
        <input class="submit_button" type = "reset" /></p>


</form>


<script>
    $('#datejs').prop('readonly', true);
    $('#datejs').removeAttr("placeholder");
    $('#datejs').attr('id','datepicker');

</script>


</body>
</html>