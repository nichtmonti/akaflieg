<?php

require_once ('php/make_pdf.php');
//erstellen der Graphen
require_once('graphen/relativbaustunden.php');
require_once('graphen/gesamtbaustunden.php');
require_once('db/database_connect.php');

$link=connect();

$sql = "SELECT * FROM `pdf`";

$result=mysqli_query($link,$sql);
disconnect($link);

$pdf=Overview_Table($result);
$pdf->AddPage("L");
$pdf->centerImage('graphen/gesamtbaustunden.png');

$pdf->AddPage("L");
$pdf->centerImage('graphen/relativbaustunden.png');
$pdf->Output();

unlink('graphen/gesamtbaustunden.png');
unlink('graphen/relativbaustunden.png');
?>
